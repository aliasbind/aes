#ifndef _KEY_EXPANSION_H_
#define _KEY_EXPANSION_H_

#include "aes.h"
#include "constants.h"
#include "operations.h"

void key_expansion(unsigned char *key, uint32_t *expanded_key);

#endif
