#include "aes.h"
#include "keyexpansion.h"
#include "operations.h"

key_length_t key_length;
int rounds;
int key_size;

static void aes_init(key_length_t key_len)
{
    key_length = key_len;

    switch (key_length) {
    case AES_128:
        key_size = 4;
        rounds = 10;
        break;
    case AES_192:
        key_size = 6;
        rounds = 12;
        break;
    case AES_256:
        key_size = 8;
        rounds = 14;
        break;
    }
}

static void aes_encrypt(unsigned char *buf, unsigned char *key)
{
    int i;
    uint32_t *expanded_key;

    expanded_key = malloc(sizeof(uint32_t) * 4 * (rounds + 1));
    key_expansion(&key[0], &expanded_key[0]);

    add_round_key(&buf[0], &expanded_key[0]);
    for (i = 0; i < rounds - 1; i++) {
        sub_bytes(&buf[0]);
        shift_rows(&buf[0]);
        mix_columns(&buf[0]);
        add_round_key(&buf[0], &expanded_key[(i+1)*4]);
    }

    sub_bytes(&buf[0]);
    shift_rows(&buf[0]);
    add_round_key(&buf[0], &expanded_key[(i+1)*4]);

    free(expanded_key);
}

void aes_128_encrypt(unsigned char *buf, unsigned char *key)
{
    aes_init(AES_128);
    aes_encrypt(buf, key);
}

void aes_192_encrypt(unsigned char *buf, unsigned char *key)
{
    aes_init(AES_192);
    aes_encrypt(buf, key);
}

void aes_256_encrypt(unsigned char *buf, unsigned char *key)
{
    aes_init(AES_256);
    aes_encrypt(buf, key);
}
