#include "keyexpansion.h"

void key_expansion(unsigned char *key, uint32_t *expanded_key)
{
    int i, j;
    uint32_t temp = 0, rconw;

    for (i = 0; i < key_size; i++) {
        expanded_key[i] = 0;
        for (j = 0; j < 4; j++) {
            expanded_key[i] <<= 8;
            expanded_key[i] |= key[4*i+j];
        }
    }

    for (i = key_size; i < 4 * (rounds + 1); i++) {
        temp = expanded_key[i-1];
        rconw = ((uint32_t)(rcon(i / key_size)) << 24);
        if (i % key_size == 0)
            temp = subword(rotword(temp)) ^ rconw;
        else if (key_size > 6 && i % key_size == 4)
            temp = subword(temp);
        expanded_key[i] = expanded_key[i - key_size] ^ temp;
    }
}
