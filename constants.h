#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

unsigned char rcon(unsigned char x);

unsigned char sbox(unsigned char x);

unsigned char gmul(unsigned char a, unsigned char b);

#endif
