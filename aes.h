#ifndef _AES_H_
#define _AES_H_

#include <stdint.h>
#include <stdlib.h>

typedef enum {
    AES_128,
    AES_192,
    AES_256,
} key_length_t;

extern key_length_t key_length;
extern int rounds;
extern int key_size;

void aes_128_encrypt(unsigned char *buf, unsigned char *key);
void aes_192_encrypt(unsigned char *buf, unsigned char *key);
void aes_256_encrypt(unsigned char *buf, unsigned char *key);

#endif
