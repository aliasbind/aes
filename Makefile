PROG=aes
OBJS=main.o constants.o keyexpansion.o operations.o aes.o
CFLAGS=-ggdb -Wall

all : $(PROG)

$(PROG): $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) -o $(PROG)

%.o : %.c
	$(CC) $(CFLAGS) -c $^

clean:
	$(RM) $(OBJS) $(PROG)
