#include "constants.h"

#define RCON_INITIALIZED    0x01
#define SBOX_INITIALIZED    0x02

static unsigned char _rcon[256];
static unsigned char _sbox[256];
static int flags;

/* Multiplies x by 2^times in GF(2^8). */
static unsigned char xtime(unsigned char x)
{
    if (x & 0x80)
        x = (x << 1) ^ 0x1b;
    else
        x <<= 1;

    return x;
}

/* Multiplies a with b in GF(2^8) */
unsigned char gmul(unsigned char a, unsigned char b)
{
    int i;
    unsigned char rez = 0;

    for (i = 0; i < 8; i++) {
        if (b & 1)
            rez ^= a;

        a = xtime(a);
        b >>= 1;
    }

    return rez;
}

unsigned char rcon(unsigned char x)
{
    int i;

    if (!(flags & RCON_INITIALIZED)) {
        _rcon[1] = 1;
        
        for (i = 2; i < 256; i++)
            _rcon[i] = xtime(_rcon[i-1]);

        _rcon[0] = _rcon[255];
        flags |= RCON_INITIALIZED;
    }

    return _rcon[x];
}

static unsigned char glongdiv(unsigned char x, unsigned char *res_bit)
{
    unsigned char out_bit = 1;

    if (x == 0)
        return 0;

    while (!(x & 0x80)) {
        out_bit <<= 1;
        x <<= 1;
    }

    *res_bit = (out_bit << 1);
    return (x << 1) ^ 0x1b;
}

static unsigned char leftmost(unsigned char x)
{
    unsigned char i = 1;

    if (x == 0)
        return 0;

    x >>= 1;
    while (x) {
        i <<= 1;
        x >>= 1;
    }

    return i;
}

static void gdiv(int do_long_div, unsigned char x, unsigned char y,
        unsigned char *rem, unsigned char *quo)
{
    unsigned char quobit = 1, leftmostbit;
    unsigned char _quo = 0, shift_y;

    if (x == 0) {
        *rem = 0;
        *quo = 0;
        return;
    }

    if (do_long_div)
        x = glongdiv(y, &_quo);

    while (leftmost(x) >= leftmost(y)) {
        leftmostbit = leftmost(x);
        shift_y = y;
        while (!(leftmostbit & shift_y)) {
            shift_y <<= 1;
            quobit <<= 1;
        }
        x ^= shift_y;
        _quo |= quobit;
        quobit = 1;
    }

    *quo = _quo;
    *rem = x;
}

static unsigned char inv(unsigned char x)
{
    unsigned char quot;
    unsigned char x1, x2, r1, r2;
    unsigned char xc = 1, rc;

    int do_long_div = 1;

    if (x == 0)
        return 0;

    x1 = 0;
    x2 = 1;

    r1 = 0x1b;
    r2 = x;

    while (r2 > 1) {
        gdiv(do_long_div, r1, r2, &rc, &quot);
        do_long_div = 0;

        xc = gmul(quot, x2) ^ x1;

        r1 = r2;
        r2 = rc;

        x1 = x2;
        x2 = xc;
    }

    return xc;
}

static unsigned char bit(unsigned char x, unsigned char pos)
{
    return (x >> (pos % 8)) & 1;
}

static unsigned char sbox1(unsigned char x)
{
    unsigned char i;
    unsigned char rez = 0;

    x = inv(x);

    for (i = 0; i < 8; i++)
        rez |= ((bit(x, i) ^ bit(x, i+4) ^ bit(x, i+5) ^
                bit(x, i+6) ^ bit(x, i+7) ^ bit(0x63, i)) << i);

    return rez;
}

unsigned char sbox(unsigned char x)
{
    unsigned char i;

    if (!(flags & SBOX_INITIALIZED)) {
        _sbox[0] = 0x63;
        for (i = 1; i != 0; i++)
            _sbox[i] = sbox1(i);
        flags |= SBOX_INITIALIZED;
    }

    return _sbox[x];
}
