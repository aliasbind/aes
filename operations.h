#ifndef _OPERATIONS_H_
#define _OPERATIONS_H_

#include "aes.h"

uint32_t subword(uint32_t block);

uint32_t rotword(uint32_t block);

void add_round_key(unsigned char *buf, uint32_t *expanded_key);

void sub_bytes(unsigned char *buf);

void shift_rows(unsigned char *buf);

void mix_columns(unsigned char *buf);

#endif
