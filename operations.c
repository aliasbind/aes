#include "aes.h"
#include "operations.h"
#include "constants.h"

uint32_t subword(uint32_t block)
{
    uint32_t temp = 0, i;
    unsigned char curr_val;

    for (i = 0; i < 4; i++) {
        curr_val = sbox((unsigned char) block);
        temp |= (((unsigned char) curr_val) << (i * 8));
        block >>= 8;
    }

    return temp;
}

uint32_t rotword(uint32_t block)
{
    unsigned char aux = 0;

    aux |= (unsigned char) (block >> 24);
    block <<= 8;
    block |= aux;

    return block;
}

void add_round_key(unsigned char *buf, uint32_t *expanded_key)
{
    int i, j;
    unsigned char temp[4];

    for (i = 0; i < 4; i++)
        for (j = 0; j < 4; j++) {
            temp[j] = (unsigned char) (expanded_key[i] >> ((3-j) * 8));
            buf[4*i+j] ^= temp[j];
        }
}

void sub_bytes(unsigned char *buf)
{
    int i;

    for (i = 0; i < 16; i++)
        buf[i] = sbox(buf[i]);
}

static void shift(unsigned char *buf, int rounds)
{
    int i, j;
    unsigned char aux;

    for (i = 0; i < rounds; i++) {
        aux = buf[0];
        for (j = 0; j < 3; j++)
            buf[j] = buf[j+1];
        buf[3] = aux;
    }
}

void shift_rows(unsigned char *buf)
{
    int i, j;
    unsigned char row[4];

    for (i = 1; i < 4; i++) {
        for (j = 0; j < 4; j++)
            row[j] = buf[i+j*4];

        shift(&row[0], i);

        for (j = 0; j < 4; j++)
            buf[i+j*4] = row[j];
    }
}

void mix_columns(unsigned char *buf)
{
    int i, j;
    unsigned char col[4];

    for (i = 0; i < 4; i++) {
        col[0] = gmul(0x02, buf[i*4]) ^ gmul(0x03, buf[i*4+1]) ^
            buf[i*4+2] ^ buf[i*4+3];
        col[1] = buf[i*4] ^ gmul(0x02, buf[i*4+1]) ^
            gmul(0x03, buf[i*4+2]) ^ buf[i*4+3];
        col[2] = buf[i*4] ^ buf[i*4+1] ^
            gmul(0x02, buf[i*4+2]) ^ gmul(0x03, buf[i*4+3]);
        col[3] = gmul(0x03, buf[i*4]) ^ buf[i*4+1] ^
            buf[i*4+2] ^ gmul(0x02, buf[i*4+3]);
        
        for (j = 0; j < 4; j++)
            buf[i*4+j] = col[j];
    }
}
